#ifndef ROCKET_H
#define ROCKET_H
#include "vector.h"

typedef struct rocket rocket_t;
struct rocket {
	vector_t position;
	vector_t velocity;
	vector_t acceleration;
	int impact;
};

rocket_t *new_rocket(vector_t);
void step_rocket(void*, void*);
void print_rocket(void*, void*);
void draw_rocket(void*, void*);

#endif
