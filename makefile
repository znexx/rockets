SHELL=/bin/sh
CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -Og -g -I .
LDFLAGS=`sdl2-config --cflags --libs` -lm -pedantic -Wall -Wextra
DISASM=objdump
DISASMFLAGS=-d -S -M intel
SRC=audio.c graphics.c input.c singly_linked_list.c rocket.c vector.c wall.c main.c
BIN=$(notdir $(shell pwd))

$(BIN): $(SRC)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

run: $(BIN)
	./$(BIN)

run_verbose: $(BIN)
	./$(BIN) -v

disassemble:
	$(DISASM) $(DISASMFLAGS) $(BIN)

clean:
	rm -rf *.o $(BIN)
