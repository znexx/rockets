#include <SDL2/SDL.h>
#include <SDL2/SDL_render.h>
#include "graphics.h"
#include "rocket.h"
#include "wall.h"
#include "vector.h"
#include "singly_linked_list.h"

extern list_t *rockets;
extern list_t *walls;

const int WINDOW_WIDTH = 640;
const int WINDOW_HEIGHT = 480;

SDL_Window	*window;
SDL_Renderer	*renderer;
SDL_GLContext	gl_context;

int graphics_init(char *title) {
	SDL_InitSubSystem(SDL_INIT_VIDEO);

	if(SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_INPUT_FOCUS, &window, &renderer)) {
		SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "Failed to create window and renderer: %s", SDL_GetError());
		return -1;
	}
	SDL_SetWindowTitle(window, title);

	if(SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND)) {
		SDL_LogError(SDL_LOG_CATEGORY_VIDEO, "Failed to set blendmode: %s", SDL_GetError());
		return -2;
	}

	gl_context = SDL_GL_CreateContext(window);
	if(!gl_context) {
		SDL_LogError(SDL_LOG_CATEGORY_VIDEO, "Failed to get OpenGL context: %s", SDL_GetError());
	}

	SDL_GL_SetSwapInterval(1);

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);

	return 0;
}

int graphics_term(void) {
	SDL_GL_DeleteContext(gl_context);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_QuitSubSystem(SDL_INIT_VIDEO);
	return 0;
}

void draw(uint32_t *ticks_end) {
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(renderer, NULL);

	SDL_SetRenderDrawColor(renderer, 0, 127, 0, 255);
	apply_to_each(draw_wall, walls, renderer);
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 127);
	apply_to_each(draw_rocket, rockets, renderer);

	(*ticks_end) = SDL_GetTicks();

	SDL_RenderPresent(renderer);
}
