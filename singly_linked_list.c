#include <stdio.h>
#include <stdlib.h>
#include "singly_linked_list.h"

void append_list(list_t *list, void* data) {
	if(!list) {
		return;
	}

	element_t *new = malloc(sizeof(element_t));
	new->next = NULL;
	new->data = data;

	if(!list->head) {
		list->head = new;
		list->tail = new;
		return;
	}

	list->tail->next = new;
	list->tail = new;
}

list_t *new_list(void) {
	return calloc(1, sizeof(list_t));
}

void print_list_debug(list_t *list) {
	if(!list) {
		return;
	}
	int n = 0;
	printf("Head: %p\nTail: %p\n", (void*)list->head, (void*)list->tail);
	element_t *elem = list->head;
	while(elem) {
		printf("%d: elem: %p next: %p data: %p\n", n, (void*)elem, (void*)elem->next, elem->data);
		elem = elem->next;
		n++;
	}
}

void* find_first(int compare_function(void*, void*), list_t *list, void *against) {
	if(!list) {
		return NULL;
	}
	element_t *elem = list->head;
	while(elem) {
		if (compare_function(elem->data, against)) {
			return elem->data;
		}
		elem = elem->next;
	}
	return NULL;
}

void apply_to_each(void apply_function(void*, void*), list_t *list, void* extra) {
	if(!list) {
		return;
	}
	element_t *elem = list->head;
	while(elem) {
		apply_function(elem->data, extra);
		elem = elem->next;
	}
}

void free_list(list_t **reference) {
	if(reference == NULL) {
		return;
	}
	list_t *list = (*reference);
	if(list->head) {
		element_t *elem = list->head;
		element_t *next;
		while(elem && elem != list->tail->next) {
			next = elem->next;
			free(elem->data);
			free(elem);
			elem = next;
		}
	}
	reference = NULL;
}
