#ifndef SINGLY_LINKED_LIST_H
#define SINGLY_LINKED_LIST_H

typedef struct element {
	void *data;
	struct element *next;
} element_t;

typedef struct list list_t;
struct list{
	element_t *head;
	element_t *tail;
};

void append_list(list_t*, void*);
list_t *new_list(void);
void *find_first(int compare_function(void*, void*), list_t*, void*);
void apply_to_each(void apply_function(void*, void*), list_t*, void*);
void free_list(list_t**);
#endif
