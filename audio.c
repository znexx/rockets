#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_log.h>
#include "audio.h"

#define BUFFER_SIZE 64

SDL_AudioDeviceID	audio_device;
int16_t			sample_buffer[BUFFER_SIZE];

void get_samples(void *userdata, uint8_t *stream, int len) {
	userdata = userdata;
	int16_t *sdl_sample_buffer = (int16_t*) stream;
	for(int x = 0; x < len/2; x++) {
		sdl_sample_buffer[x] = sample_buffer[x];
	}
}

int audio_init() {
	SDL_InitSubSystem(SDL_INIT_AUDIO);

	// https://wiki.libsdl.org/SDL_OpenAudioDevice
	// https://wiki.libsdl.org/SDL_AudioSpec
	SDL_AudioSpec	want, have;
	SDL_zero(want);

	want.freq	= 22050;
	want.format	= AUDIO_S16;
	want.channels	= 1;
	want.samples	= BUFFER_SIZE;
	want.callback	= get_samples;
	want.userdata	= NULL;

	audio_device = SDL_OpenAudioDevice(NULL, 0, &want, &have, 0);
	if(!audio_device)
	{
		SDL_LogCritical(SDL_LOG_CATEGORY_AUDIO, "Failed to open audio device: %s", SDL_GetError());
		return -1;
	}
	SDL_PauseAudioDevice(audio_device, 0);
	return 0;
}

int audio_term(void) {
	SDL_CloseAudioDevice(audio_device);
	return 0;
}
