#ifndef INPUT_H
#define INPUT_H
#include <stdbool.h>

int input_init(void);
void handle_input(void);

#endif
