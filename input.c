#include <SDL2/SDL.h>
#include <SDL2/SDL_log.h>
#include <SDL2/SDL_events.h>
#include <signal.h>
#include <stdbool.h>
#include "keyboard_actions.c"

extern bool running;

static void (*key_up)(SDL_Keymod);
static void (*key_down)(SDL_Keymod);
static void (*key_left)(SDL_Keymod);
static void (*key_right)(SDL_Keymod);

static void stop_running(){
	SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "Exiting");
	running = false;
}

static void set_keys(void (up(SDL_Keymod)), void (down(SDL_Keymod)), void (left(SDL_Keymod)), void (right(SDL_Keymod))) {
	key_up = up;
	key_down = down;
	key_left = left;
	key_right = right;
}

int input_init(void) {
	SDL_InitSubSystem(SDL_INIT_EVENTS);

	signal(SIGINT, stop_running);

	set_keys(empty_action, empty_action, empty_action, empty_action);

	return 0;
}

static void handle_keyboard(int sym) {
	SDL_Keymod	mod = SDL_GetModState();
	switch(sym) {
		case SDLK_ESCAPE:
		stop_running();
		break;

		case SDLK_LEFT:
		key_left(mod);
		break;

		case SDLK_RIGHT:
		key_right(mod);
		break;

		case SDLK_UP:
		key_up(mod);
		break;

		case SDLK_DOWN:
		key_down(mod);
		break;
	}
}

void handle_input(void) {
	SDL_Event	event;
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_QUIT:
			stop_running();
			break;
			case SDL_KEYDOWN:
			handle_keyboard(event.key.keysym.sym);
			break;
		}
	}
}
