#ifndef GRAPHICS_H
#define GRAPHICS_H

int graphics_init(char*);
int graphics_term(void);
void draw(uint32_t*);

#endif
