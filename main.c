#include <SDL2/SDL.h>

#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "graphics.h"
#include "audio.h"
#include "input.h"
#include "singly_linked_list.h"
#include "rocket.h"
#include "vector.h"
#include "wall.h"

bool	running = true;

list_t	*rockets;
list_t	*walls;

static void loop(void) {
	while(running) {
		uint32_t ticks_start = SDL_GetTicks();
		uint32_t ticks_end;

		handle_input();
		apply_to_each(step_rocket, rockets, walls);

		draw(&ticks_end);

		//apply_to_each(print_rocket, rockets, NULL);
		SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Loop time: %d ms\n", ticks_end - ticks_start);
	}
}

static int init(char *title) {
	SDL_Init(0);

	srand(time(NULL));

	if(graphics_init(title))
		return -1;

	if(audio_init())
		return -2;

	if(input_init())
		return -3;

	rockets = new_list();
	for(int n=0; n<1000; n++) {
		append_list(rockets, new_rocket((vector_t){320, 32}));
	}
	//apply_to_each(print_rocket, rockets, NULL);

	walls = new_list();
	append_list(walls, new_wall((vector_t){0, 16}, (vector_t){639, 16}));
	append_list(walls, new_wall((vector_t){16, 0}, (vector_t){16, 479}));
	append_list(walls, new_wall((vector_t){623, 0}, (vector_t){623, 479}));
	append_list(walls, new_wall((vector_t){0, 463}, (vector_t){639, 463}));
	append_list(walls, new_wall((vector_t){633, 403}, (vector_t){563, 473}));
	append_list(walls, new_wall((vector_t){rand()%640, rand()%480}, (vector_t){rand()%640, rand()%480}));
	append_list(walls, new_wall((vector_t){rand()%640, rand()%480}, (vector_t){rand()%640, rand()%480}));
	append_list(walls, new_wall((vector_t){rand()%640, rand()%480}, (vector_t){rand()%640, rand()%480}));
	append_list(walls, new_wall((vector_t){rand()%640, rand()%480}, (vector_t){rand()%640, rand()%480}));
	apply_to_each(print_wall, walls, NULL);

	return 0;
}

static int term(void) {
	free_list(&walls);
	free_list(&rockets);

	audio_term();
	graphics_term();
	SDL_Quit();
	return 0;
}

int main(int argc, char *argv[]) {
	argc = argc;
	int ret = 0;

	SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);

	ret = init(argv[0]);
	if(!ret)
		loop();

	ret = term();

	return ret;
}
