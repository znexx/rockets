#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <stdio.h>
#include <stdlib.h>
#include "vector.h"
#include "wall.h"
#include "singly_linked_list.h"
#include "rocket.h"

rocket_t *new_rocket(vector_t position) {
	rocket_t *rocket = malloc(sizeof(rocket_t));
	rocket->position = position;
	rocket->velocity = random_polar_vector(1.0f);
	rocket->acceleration = (vector_t){0, 0.01f};
	return rocket;
}

int check_wall(void *wall_ptr, void *rocket_ptr) {
	wall_t *wall = wall_ptr;
	rocket_t *rocket = rocket_ptr;

	vector_t next = (vector_t){rocket->position.x + rocket->velocity.x, rocket->position.y + rocket->velocity.y};
	return is_intersect(&rocket->position, &next, &wall->v1, &wall->v2);
}

void step_rocket(void *rocket_ptr, void *walls_ptr) {
	list_t *walls = walls_ptr;
	rocket_t *rocket = rocket_ptr;

	rocket->impact = 0;
	wall_t *impact = find_first(check_wall, walls, rocket);
	if(impact) {
		float dx = rocket->velocity.x;
		float dy = rocket->velocity.y;
		float nx = impact->normal.x;
		float ny = impact->normal.y;
		float dot = dx*nx + dy*ny;
		rocket->velocity.x = dx - 2.0f*dot*nx;
		rocket->velocity.y = dy - 2.0f*dot*ny;
		//rocket->velocity.x *= 0.5;
		//rocket->velocity.y *= 0.5;
		rocket->impact = 1;
	}

	rocket->position.x += rocket->velocity.x;
	rocket->position.y += rocket->velocity.y;

	rocket->velocity.x += rocket->acceleration.x;
	rocket->velocity.y += rocket->acceleration.y;
}

void print_rocket(void *rocket_ptr, void *ptr2) {
	ptr2 = ptr2;
	rocket_t *rocket = rocket_ptr;
	printf("Rocket: x: %3.2f y: %3.2f dx: %3.2f dy: %3.2f %s\n", rocket->position.x, rocket->position.y, rocket->velocity.x, rocket->velocity.y, rocket->impact?"IMPACT":"");
}

void draw_rocket(void *rocket_ptr, void *ptr2) {
	rocket_t *rocket = rocket_ptr;
	SDL_Renderer *renderer = ptr2;

	SDL_RenderDrawLine(renderer, rocket->position.x, rocket->position.y,
	rocket->position.x - rocket->velocity.x, rocket->position.y - rocket->velocity.y
	);
}
